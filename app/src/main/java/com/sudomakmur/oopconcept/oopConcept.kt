package com.sudomakmur.oopconcept

open class Manusia(nama : String){
    var nama : String? = ""
    init {
        this.nama = nama
    }

    open fun makan(){
        println("${this.nama} makan nasi ")
    }
}

class ManusiaMilenial(nama : String) : Manusia(nama) {
    var emailAttribut : String? = ""
    private var passwordAttribut : String? =""

    fun setEmail(emailParamater : String?){
        this.emailAttribut = emailParamater
    }

    fun setPassword(password : String?){
        this.passwordAttribut = password?.replace("a","*")
    }

    fun setPassword(password: String? , enkripsi : String){

    }

    fun getPassword() : String?{
        return this.passwordAttribut
    }

    override fun makan() {
        println("gasuka makan gamau gasuka gelay")
    }

    fun info(){
        println("${this.nama} mempunyai email dengan alamat : ${this.emailAttribut} dengan password : ${this.passwordAttribut} ")
    }
}



//abstract class Buah {
//    private var warna : String = ""
//
//    abstract fun caraMakan()
//
//    fun setWarna(warna : String){
//        this.warna = warna
//    }
//}


interface BuahInteface {

    fun pilihWarna(warna : String)
    fun pilihUkuran(ukuran : String)
    fun pilihJenis(jenis : String)
    fun pilihBerat(berat : Int)
}

class Durian : BuahInteface{

    override fun pilihWarna(warna: String) {
        println("Warna Buah Durian nya $warna")
    }

    override fun pilihUkuran(ukuran: String){
        println("Ukuran Duriannya $ukuran")
    }

    override fun pilihJenis(jenis: String){
        println("Jenisnya Durian $jenis")
    }

    override fun pilihBerat(berat: Int) {
        println("Beratnya $berat Kg")
    }

}


class AbangTukang {
    var buahInterface : BuahInteface

    init {
        buahInterface = Durian()
    }

    fun pilihWarna(value : String){
        buahInterface.pilihWarna(value)
    }

    fun pilihUkuran(value: String){
        buahInterface.pilihUkuran(value)
    }

    fun pilihJenis(value: String){
        buahInterface.pilihJenis(value)
    }

    fun pilihBerat(value: Int){
        buahInterface.pilihBerat(value)
    }
}


fun main(){

    val abangTukang = AbangTukang()
    abangTukang.pilihWarna("Kuning")
    abangTukang.pilihUkuran("Jumbo")
    abangTukang.pilihJenis("Montong")
    abangTukang.pilihBerat(15)

//    val programmer = ManusiaMilenial("Marfuah")
//    programmer.setEmail("prorgramer@gmail.com")
//    programmer.setPassword("rahasia123")
//    programmer.info()
//
//    val petani = ManusiaMilenial("Marjoko")
//    petani.setEmail("petani@gmail.com")
//    petani.setPassword("rahasia123")
//    petani.info()
//
//
//    val dokter = ManusiaMilenial("Jokris")
//    dokter.setEmail("dokter@gmail.com")
//    petani.setPassword("rahasia123")
//    dokter.info()





}